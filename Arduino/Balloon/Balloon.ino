// Debugging
boolean debug = true; // extra debug info?

/*
   Balloon!!!1

   
  ************************
  | Teensy 3.2 pins used |
  ************************
  D0/RX1     :  RX GPS
  D1/TX1     :  X
  D2         :  X   
  D3         :  X
  D4         :  X
  D5         :  X
  D6         :  X
  D7/RX3     :  -->  LV1 - logic level shifter - HV1  -->   RX, Telemetry (Simon)
  D8/TX3     :  <--  LV2 - logic level shifter - HV2  <--   TX, Telemetry (Simon)
  D9/RX2     :  X
  D10/TX2    :  CS, Datalogging (Jim)
  D11/DOUT   :  MOSI, Datalogging (Jim)
  D12/DIN    :  MISO, Datalogging (Jim)
  D13/LED    :  X
  A0         :  X
  A1         :  X
  A2         :  X
  A3         :  X
  A4/SDA0    :  I2C, BMP180 & MPU6050 (Jirka)
  A5(SCL0    :  I2C, BMP180 & MPU6050 (Jirka)
  A6         :  X
  A7         :  X
  A8         :  X
  A9         :  X

*/

// For telemetry
#define TXSERIAL Serial3
//NB cannot use Serial2 as it's also the CS pin

// For datalogging
#include <SPI.h>
#include <SD.h>

const int DataloggingCS = 10; // teensy preference

// For GPS
#include <TinyGPS.h>
TinyGPS gps;
void gpsdump(TinyGPS &gps);
#define GPSSERIAL Serial1


// For barometer
#include "Wire.h"
#include "I2Cdev.h"
#include "BMP085.h"

BMP085 barometer; // class default I2C address is 0x77


//global variables
int hdop;
int32_t lastMicros;
float flat, flon, temp, pressure, alt, fc, fkmph;
unsigned long age;
byte Hour, Minute, Second;
long seaLevel = 100170; // https://www.smhi.se/en/weather/sweden-weather/observations#ws=wpt-a,proxy=wpt-a,tab=vader,param=msl

char sendCharBuffer[100];  // this NEEDS to be big enough for all we're sending, otherwise the whole Teensy halts

// Phase management
static enum { PRELAUNCH, // Setup and system testing
              LAUNCH, // Off we go :)
              ASCENSION, // What we need to do to climb
              FLOAT, // Floating about
              DESCENSION, // Going back down
              ON_GROUND, // What to to when back on the ground
              EMERGENCY // Alert general!!
            } state = PRELAUNCH;




void setup() {
  // join I2C bus (I2Cdev library doesn't do this automatically)
  Wire.begin();

  Serial.begin(19200); // We agree to talk quickly!
  TXSERIAL.begin(19200); // Still Quick!!
  GPSSERIAL.begin(19200); // Same speed!

  // initialize device
  barometer.initialize();

  delay(1000);

  Serial.println("Setup");
  Serial.print("Sizeof(gpsobject) = "); Serial.println(sizeof(TinyGPS));
  Serial.println(barometer.testConnection() ? "BMP085 connection successful" : "BMP085 connection failed");


  if (!SD.begin(DataloggingCS)) {
    Serial.println("Card failed, or not present");
    return;
  }
}

void loop() {
  logAndSendSensordata();

  switch (state) {

    case PRELAUNCH: {

        if (debug) {
          Serial.print("state = ");
          Serial.println(state);
        }


        break;
      }

    case LAUNCH: {

        if (debug) {
          Serial.print("state = ");
          Serial.println(state);
        }


        break;
      }

    case ASCENSION: {

        if (debug) {
          Serial.print("state = ");
          Serial.println(state);
        }


        break;
      }

    case FLOAT: {

        if (debug) {
          Serial.print("state = ");
          Serial.println(state);
        }


        break;
      }

    case DESCENSION: {

        if (debug) {
          Serial.print("state = ");
          Serial.println(state);
        }


        break;
      }

    case ON_GROUND: {

        if (debug) {
          Serial.print("state = ");
          Serial.println(state);
        }


        break;
      }

    case EMERGENCY: {

        if (debug) {
          Serial.print("state = ");
          Serial.println(state);
        }


        break;
      }

  }

}

void logAndSendSensordata() {


  if (debug) {
    Serial.println("logAndSendSensordata()");
    delay(2000);
  }

  unsigned long start = millis();
  bool newdata = false;

  // Get update from GPS every 5 seconds, otherwise skip
  while (millis() - start < 5000) {
    if (GPSSERIAL.available()) {
      char c = GPSSERIAL.read();
      // Serial.print(c);  // uncomment to see raw GPS data
      if (gps.encode(c)) {
        newdata = true;
      }
    }
  }

  if (debug) {
    if (!newdata) {
      unsigned long chars;
      unsigned short sentences, failed;
      gps.stats(&chars, &sentences, &failed);
      Serial.print("Stats: characters: "); Serial.print(chars); Serial.print(" sentences: ");
      Serial.print(sentences); Serial.print(" failed checksum: "); Serial.println(failed);
    }
  }



  // if new GPS data exists
  if (newdata)
  {
    gpsCheck(gps);

    if (barometer.testConnection()) {
      // request temperature
      barometer.setControl(BMP085_MODE_TEMPERATURE);
      lastMicros = micros();
      while (micros() - lastMicros < barometer.getMeasureDelayMicroseconds());
      temp = barometer.getTemperatureC();

      barometer.setControl(BMP085_MODE_PRESSURE_3);
      while (micros() - lastMicros < barometer.getMeasureDelayMicroseconds());

      pressure = barometer.getPressure();
      alt = barometer.getAltitude(pressure, seaLevel);
    }
    else if (!barometer.testConnection()) {
      alt = 1337;
      temp = -23;
    }
    String logRow = String(Hour) +  String(Minute) +  String((Second - (age / 1000))) + ";" +
                    String(flat, 6) + ";" +
                    String(flon, 6) + ";" +
                    String(hdop) + ";" +
                    String(fkmph, 4) + ";" +
                    String(fc, 4) + ";" +
                    String(alt) + ";" +
                    String(temp);

    sprintf(sendCharBuffer, "%02d%02d%02d;%f;%f;%03i;%1.1f;%1.1f;%1.1f;%1.1f\n", Hour, Minute, Second, flat, flon, hdop, fkmph, fc, alt, temp);
    TXSERIAL.write(sendCharBuffer);

    // TEMP TEST
    float temp2 = 13.37; // temparature 2
    int hum = 21; // humidity
    float fGF = 1.1; //Gforce
    
     sprintf(sendCharBuffer, ";%1.1f;%i;%1.1f\n", temp2, hum, fGF); 
    logThis(sendCharBuffer);

  }
}

void logThis(String dataRow) {

  if (debug) {
    Serial.println("logThis()");
  }

  File dataFile = SD.open("log.csv", FILE_WRITE);

  if (dataFile) {
    dataFile.println(dataRow);
    dataFile.close();
  } else {
    Serial.println("error opening log.csv");
  }

}


void gpsCheck(TinyGPS &gps)
{
  gps.crack_datetime(NULL, NULL, NULL, &Hour, &Minute, &Second, NULL, &age);
  gps.f_get_position(&flat, &flon, &age);
  fc = gps.f_course();
  fkmph = gps.f_speed_kmph();
  hdop = gps.hdop();

  if (debug) {
    static char tempChar[22];
    Serial.print("GPS data received: ");
    sprintf(tempChar, "%f;%f\n", flat, flon);
    Serial.print(tempChar);
  }
}
