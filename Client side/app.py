#!/usr/bin/python

import time
import serial
import requests

sleep = 5 # Time to wait between pushes (seconds)
increment = 1;
serial_port = "/dev/cu.wchusbserial1410"
request_url = "http://demo.almbay.com/balloon/receive.php"

ser = serial.Serial(serial_port, 19200, timeout=1);

while True:
    serial_data = ser.readline().strip()
    payload_array = serial_data.split(";")

    if len(payload_array) == 8:
    	print "Serial request #%d" % increment

    	payload = {
    		"time": payload_array[0],
    		"lat": payload_array[1],
    		"lon": payload_array[2],
    		"hdop": payload_array[3],
    		"speed": payload_array[4],
    		"course": payload_array[5],
    		"alt": payload_array[6],
    		"temp": payload_array[7],
            "live": True
    	}

    	request = requests.post(request_url, data=payload)
    	if request.status_code == requests.codes.ok:
    		print "Request response: %s \n" % request.text
        else:
			print "Request failed, server response: %s \n" % request.text

    	increment += 1;
    	time.sleep(sleep)
