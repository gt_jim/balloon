import time
import serial
import requests
import csv

sleep = 0.1 # Time to wait between pushes (seconds)
increment = 1;
request_url = "http://demo.almbay.com/balloon/receive.php"


data = open('/Users/Simon/Downloads/path.csv')
reader = csv.reader(data)
info = list(reader)


for i in range(2,len(info)):
    payload = {
        "time": 0,
        "lat": info[i][2],
        "lon": info[i][3],
        "hdop": 0,
        "speed": 0,
        "course": 0,
        "alt": info[i][4],
        "temp": 0,
        "live": False
    }
    request = requests.post(request_url, data=payload)
    if request.status_code == requests.codes.ok:
        print "Request response: %s \n" % request.text
    else:
	    print "Request failed, server response: %s \n" % request.text
print "Done sending predicted data"
raise SystemExit(0)
